#include <stdio.h>

int main(void){
    for(int i = 1; i < 5; i++){
        printf("VAMOS JOGAR!!\n");
        int numerosecreto = 42;
        int chute = numerosecreto;
        printf("Digite seu chute\n");
        scanf("%d", &chute);
        if(chute < 0){
            printf("Jogada Inválida\n");
            i--;
            continue;
        }
        else{
            if(chute == numerosecreto){
                printf("Parabéns, Você Acertou\n");
                break;
            }
            else if (chute > numerosecreto){
                printf("Tente um número menor\n");
            }
            else{
                printf("Tente um número maior\n");
            }
        } 
    }
    printf("FIM DE JOGO!!\n");
}